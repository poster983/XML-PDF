# XML-PDF
High Level XML file to PDF file converter with CLI

## INSTALL

With NPM:
`npm install xml-pdf -g`

Without NPM:
Download and unzip the repo
Run: `npm install -g`

## USAGE   
  
Usage: `xml-pdf [options] <xml> <template> <output>`  
  

  Options:  
```
    -h, --height <height>            Height of the pdf.  Not to be used with "--format" and "--orientation"  Allowed units: mm, cm, in, px  
    -w, --width <width>              Width of the pdf.  Not to be used with "--format" and "--orientation"  Allowed units: mm, cm, in, px  
    -f, --format <format>            Not to be used with "--height" and "--width" Allowed units: A3, A4, A5, Legal, Letter, Tabloid  
    -o, --orientation <orientation>  portrait or landscape.  Not to be used with "--height" and "--width"  
    -v, --verbose                    Enables verbose mode. Helpful when setting up the template.  
    -h, --help                       output usage information  
```